package com.myk.gitrunnertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitrunnertestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitrunnertestApplication.class, args);
    }

}
